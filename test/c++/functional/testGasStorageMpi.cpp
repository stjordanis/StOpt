// Copyright (C) 2016 Fime
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#define BOOST_TEST_DYN_LINK
#define _USE_MATH_DEFINES
#include <math.h>
#include <functional>
#include <memory>
#include <boost/test/unit_test.hpp>
#include <boost/timer/timer.hpp>
#include <boost/mpi.hpp>
#include <Eigen/Dense>
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/core/grids/SparseSpaceGridBound.h"
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/RegularLegendreGridGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "test/c++/tools/simulators/MeanRevertingSimulator.h"
#include "test/c++/tools/dp/DynamicProgrammingByRegressionDist.h"
#include "test/c++/tools/dp/DynamicProgrammingByRegressionSparse.h"
#include "test/c++/tools/dp/SimulateRegressionDist.h"
#include "test/c++/tools/dp/SimulateRegressionControlDist.h"
#include "test/c++/tools/dp/SimulateRegression.h"
#include "test/c++/tools/dp/SimulateRegressionControl.h"
#include "test/c++/tools/dp/OptimizeGasStorage.h"

using namespace std;
using namespace Eigen ;
using namespace StOpt;

double accuracyClose = 1.5;
double accuracyEqual = 0.0001;

/// For Clang < 3.7 (and above ?) to be compatible GCC 5.1 and above
namespace boost
{
namespace unit_test
{
namespace ut_detail
{
string normalize_test_case_name(const_string name)
{
    return (name[0] == '&' ? string(name.begin() + 1, name.size() - 1) : string(name.begin(), name.size()));
}
}
}
}

class ZeroFunction
{
public:
    ZeroFunction() {}
    double operator()(const int &, const ArrayXd &, const ArrayXd &) const
    {
        return 0. ;
    }
};



#if defined   __linux
#include <fenv.h>
#define enable_abort_on_floating_point_exception() feenableexcept(FE_DIVBYZERO | FE_INVALID)
#endif

template< class Grid>
double  testGasStorage(shared_ptr< Grid > &p_grid, const double &p_maxLevelStorage, const    bool p_bOneFile)
{
    // storage
    /////////
    double injectionRateStorage = 60000;
    double withdrawalRateStorage = 45000;
    double injectionCostStorage = 0.35;
    double withdrawalCostStorage = 0.35;

    double maturity = 1.;
    size_t nstep = 10;
    // define  a time grid
    shared_ptr<OneDimRegularSpaceGrid> timeGrid(new OneDimRegularSpaceGrid(0., maturity / nstep, nstep));
    // future values
    shared_ptr<vector< double > > futValues(new vector<double>(nstep + 1));
    // periodicity factor
    int iPeriod = 52;
    for (size_t i = 0; i < nstep + 1; ++i)
        (*futValues)[i] = 50. + 20 * sin((M_PI * i * iPeriod) / nstep);
    // define the future curve
    shared_ptr<OneDimData<OneDimRegularSpaceGrid, double> > futureGrid(new OneDimData< OneDimRegularSpaceGrid, double> (timeGrid, futValues));
    // one dimensional factors
    int nDim = 1;
    VectorXd sigma = VectorXd::Constant(nDim, 0.94);
    VectorXd mr = VectorXd::Constant(nDim, 0.29);
    // number of simulations
    size_t nbsimulOpt = 20000;
    // no actualization
    double r = 0 ;
    // a backward simulator
    ///////////////////////
    bool bForward = false;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > backSimulator(new	  MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulOpt, bForward));
    // optimizer
    ///////////
    shared_ptr< OptimizeGasStorage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> >  > > storage(new  OptimizeGasStorage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> >  > (injectionRateStorage, withdrawalRateStorage, injectionCostStorage, withdrawalCostStorage));
    // regressor
    ///////////
    int nMesh = 6;
    ArrayXi nbMesh = ArrayXi::Constant(1, nMesh);
    shared_ptr< BaseRegression > regressor(new LocalLinearRegression(nbMesh));
    // final value
    function<double(const int &, const ArrayXd &, const ArrayXd &)>   vFunction = ZeroFunction();

    // initial values
    ArrayXd initialStock = ArrayXd::Constant(1, p_maxLevelStorage);
    int initialRegime = 0; // only one regime

    boost::mpi::communicator world;
    // Optimize
    ///////////
    string fileToDump = "CondExp";
    // link the simulations to the optimizer
    storage->setSimulator(backSimulator);
    double valueOptimDist =  DynamicProgrammingByRegressionDist(p_grid, storage, regressor, vFunction, initialStock, initialRegime, fileToDump, p_bOneFile);

    world.barrier();

    // a forward simulator
    ///////////////////////
    int nbsimulSim = 40000;
    bForward = true;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator(new	  MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulSim, bForward));
    // link the simulations to the optimizer
    storage->setSimulator(forSimulator);
    double valSimuDist = SimulateRegressionDist(p_grid, storage, vFunction, initialStock, initialRegime, fileToDump, p_bOneFile) ;

    if (world.rank() == 0)
    {
        BOOST_CHECK_CLOSE(valueOptimDist, valSimuDist, accuracyClose);
        cout << " Optim " << valueOptimDist << " valSimuDist " << valSimuDist <<  endl ;
    }

    /// a second forward simulator
    /////////////////////////////
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator2 = make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulSim, bForward);

    double valSimuDist2 ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(forSimulator2);
        valSimuDist2 = SimulateRegressionControlDist(p_grid, storage, vFunction, initialStock, initialRegime, fileToDump, p_bOneFile) ;

    }
    if (world.rank() == 0)
    {
        cout << " valSimuDist2  " << valSimuDist2 << " valueOptimDist " << valueOptimDist << endl ;
        BOOST_CHECK_CLOSE(valueOptimDist, valSimuDist2, accuracyClose);
    }
    return valueOptimDist;
}


BOOST_AUTO_TEST_CASE(testSimpleStorageDist)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    int nGrid = 10;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    shared_ptr<RegularSpaceGrid> grid = make_shared<RegularSpaceGrid>(lowValues, step, nbStep);
    bool bOneFile = true ;
    double val = testGasStorage(grid, maxLevelStorage, bOneFile);

    boost::mpi::communicator world;
    world.barrier();

    // grid
    //////
    ArrayXi poly = ArrayXi::Constant(1, 1);
    shared_ptr<RegularLegendreGrid> gridL = make_shared<RegularLegendreGrid>(lowValues, step, nbStep, poly);

    double valLegendre = testGasStorage(gridL, maxLevelStorage, bOneFile);

    if (world.rank() == 0)
    {
        BOOST_CHECK_CLOSE(val, valLegendre, accuracyEqual) ;
    }
}

BOOST_AUTO_TEST_CASE(testSimpleStorageMultipleFileDist)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    int nGrid = 10;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    shared_ptr<RegularSpaceGrid> grid = make_shared<RegularSpaceGrid>(lowValues, step, nbStep);
    bool bOneFile = false ;
    testGasStorage(grid, maxLevelStorage, bOneFile);
}

BOOST_AUTO_TEST_CASE(testSimpleStorageLegendreQuadraticDist)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    int nGrid = 8;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    ArrayXi poly = ArrayXi::Constant(1, 2);
    shared_ptr<RegularLegendreGrid> grid = make_shared<RegularLegendreGrid>(lowValues, step, nbStep, poly);

    bool bOneFile = false ;
    testGasStorage(grid, maxLevelStorage, bOneFile);
}

BOOST_AUTO_TEST_CASE(testSimpleStorageLegendreCubicDist)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    int nGrid = 8;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    ArrayXi poly = ArrayXi::Constant(1, 3);
    shared_ptr<RegularLegendreGrid> grid = make_shared<RegularLegendreGrid>(lowValues, step, nbStep, poly);

    bool bOneFile = true ;
    testGasStorage(grid, maxLevelStorage, bOneFile);
}

/// \brief valorization of a given gas storage on a  grid
/// \param p_grid             the grid
/// \param p_maxLevelStorage  maximum level
void testGasStorageSparseGrid(shared_ptr< SparseSpaceGrid > &p_grid, const double &p_maxLevelStorage)
{
    // storage
    /////////
    double injectionRateStorage = 60000;
    double withdrawalRateStorage = 45000;
    double injectionCostStorage = 0.35;
    double withdrawalCostStorage = 0.35;

    double maturity = 1.;
    size_t nstep = 10;
    // define  a time grid
    shared_ptr<OneDimRegularSpaceGrid> timeGrid(new OneDimRegularSpaceGrid(0., maturity / nstep, nstep));
    // future values
    shared_ptr<vector< double > > futValues(new vector<double>(nstep + 1));
    // periodicity factor
    int iPeriod = 52;
    for (size_t i = 0; i < nstep + 1; ++i)
        (*futValues)[i] = 50. + 20 * sin((M_PI * i * iPeriod) / nstep);
    // define the future curve
    shared_ptr<OneDimData<OneDimRegularSpaceGrid, double> > futureGrid(new OneDimData< OneDimRegularSpaceGrid, double> (timeGrid, futValues));
    // one dimensional factors
    int nDim = 1;
    VectorXd sigma = VectorXd::Constant(nDim, 0.94);
    VectorXd mr = VectorXd::Constant(nDim, 0.29);
    // number of simulations
    size_t nbsimulOpt = 20000;

    // no actualization
    double r = 0 ;
    // a backward simulator
    ///////////////////////
    bool bForward = false;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > backSimulator(new	  MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulOpt, bForward));
    // optimizer
    ///////////
    shared_ptr< OptimizeGasStorage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > > storage(new  OptimizeGasStorage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > >(injectionRateStorage, withdrawalRateStorage, injectionCostStorage, withdrawalCostStorage));
    // regressor
    ///////////
    int nMesh = 6;
    ArrayXi nbMesh = ArrayXi::Constant(1, nMesh);
    shared_ptr< LocalLinearRegression > regressor(new LocalLinearRegression(nbMesh));
    // final value
    function<double(const int &, const ArrayXd &, const ArrayXd &)>  vFunction = ZeroFunction();

    // initial values
    ArrayXd initialStock = ArrayXd::Constant(1, p_maxLevelStorage);
    int initialRegime = 0; // only one regime

    // Optimize
    ///////////
    string fileToDump = "CondExpGasStorage";
    double valueOptimSparse ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(backSimulator);
        boost::timer::auto_cpu_timer t;

        valueOptimSparse =  DynamicProgrammingByRegressionSparse(p_grid, storage, regressor, vFunction, initialStock, initialRegime, fileToDump);

        cout << " valueOptimSparse " << valueOptimSparse << endl ;
    }
    // a forward simulator
    ///////////////////////
    int nbsimulSim = 40000;
    bForward = true;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator = make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulSim, bForward);
    double valSimuSparse ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(forSimulator);
        boost::timer::auto_cpu_timer t;
        valSimuSparse = SimulateRegression(p_grid, storage, vFunction, initialStock, initialRegime, fileToDump) ;

    }
    boost::mpi::communicator world;
    if (world.rank() == 0)
    {
        cout << " valSimuSparse  " << valSimuSparse << " valueOptimSparse " << valueOptimSparse << endl ;
        BOOST_CHECK_CLOSE(valueOptimSparse, valSimuSparse, accuracyClose);
    }

    /// a second forward simulator
    /////////////////////////////
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator2 = make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > (futureGrid, sigma, mr, r,  maturity, nstep, nbsimulSim, bForward);

    double valSimuSparse2 ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(forSimulator2);
        boost::timer::auto_cpu_timer t;
        valSimuSparse2 = SimulateRegressionControl(p_grid, storage, vFunction, initialStock, initialRegime, fileToDump) ;

    }
    if (world.rank() == 0)
    {
        cout << " valSimuSparse2  " << valSimuSparse2 << " valueOptimSparse " << valueOptimSparse << endl ;
        BOOST_CHECK_CLOSE(valueOptimSparse, valSimuSparse2, accuracyClose);
    }
}

BOOST_AUTO_TEST_CASE(testSimpleStorageSparse)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd sizeDomain = ArrayXd::Constant(1, maxLevelStorage);
    ArrayXd  weight = ArrayXd::Constant(1, 1.);
    int level = 4;
    size_t degree = 1;
    shared_ptr<SparseSpaceGrid> grid = make_shared<SparseSpaceGridBound>(lowValues, sizeDomain, level, weight, degree);

    testGasStorageSparseGrid(grid, maxLevelStorage);
}

// (empty) Initialization function. Can't use testing tools here.
bool init_function()
{
    return true;
}

int main(int argc, char *argv[])
{
#if defined   __linux
    enable_abort_on_floating_point_exception();
#endif
    boost::mpi::environment env(argc, argv);
    return ::boost::unit_test::unit_test_main(&init_function, argc, argv);
}
