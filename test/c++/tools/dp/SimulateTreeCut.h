// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef SIMULATEREGTREECUT_H
#define SIMULATEREGTREECUT_H
#include <Eigen/Dense>
#include <functional>
#include <memory>
#include "geners/BinaryFileArchive.hh"
#include "StOpt/core/grids/FullGrid.h"
#include "StOpt/tree/StateTreeStocks.h"
#include "StOpt/dp/SimulateStepTreeCut.h"
#include "StOpt/dp/OptimizerDPCutTreeBase.h"
#include "StOpt/dp/SimulatorDPBaseTree.h"


/** \file SimulateTreeCut.h
 *  \brief Defines a simple program showing how to use simulations when optimizaton achived with transition problems solved with cuts.
 *        A simple grid  is used, uncertainties are  discrete and defined on a tree
 *  \author Xavier Warin
 */

/// \brief Simulate the optimal strategy , Bellman cuts used to allow LP resolution of transition problems , uncertainties on a tree
/// \param p_grid                   grid used for  deterministic state (stocks for example)
/// \param p_optimize               optimizer defining the optimization between two time steps
/// \param p_funcFinalValue         function defining the final value cuts
/// \param p_pointStock             initial point stock
/// \param p_initialRegime          regime at initial date
/// \param p_fileToDump             name of the file used to dump continuation values in optimization
double SimulateTreeCut(const std::shared_ptr<StOpt::FullGrid> &p_grid,
                       const std::shared_ptr<StOpt::OptimizerDPCutTreeBase > &p_optimize,
                       const std::function< Eigen::ArrayXd(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>   &p_funcFinalValue,
                       const Eigen::ArrayXd &p_pointStock,
                       const int &p_initialRegime,
                       const std::string   &p_fileToDump)
{
    // from the optimizer get back the simulator
    std::shared_ptr< StOpt::SimulatorDPBaseTree> simulator = p_optimize->getSimulator();
    int nbStep = simulator->getNbStep();
    std::vector< StOpt::StateTreeStocks> states;
    states.reserve(simulator->getNbSimul());
    for (int is = 0; is < simulator->getNbSimul(); ++is)
        states.push_back(StOpt::StateTreeStocks(p_initialRegime, p_pointStock, 0));
    gs::BinaryFileArchive ar(p_fileToDump.c_str(), "r");
    // name for continuation object in archive
    std::string nameAr = "ContinuationTree";
    // cost function
    Eigen::ArrayXXd costFunction = Eigen::ArrayXXd::Zero(p_optimize->getSimuFuncSize(), simulator->getNbSimul());
    // iterate on time steps
    for (int istep = 0; istep < nbStep; ++istep)
    {
        StOpt::SimulateStepTreeCut(ar, nbStep - 1 - istep, nameAr, p_grid, p_optimize).oneStep(states, costFunction);
        // new date
        simulator->stepForward();
        for (int is = 0; is < simulator->getNbSimul(); ++is)
            states[is].setStochasticRealization(simulator->getNodeAssociatedToSim(is));
    }
    // final : accept to exercise if not already done entirely (here suppose one function to follow)
    for (int is = 0; is < simulator->getNbSimul(); ++is)
        costFunction(0, is) += p_funcFinalValue(states[is].getRegime(), states[is].getPtStock(), simulator->getValueAssociatedToNode(states[is].getStochasticRealization()))(0);
    // average gain/cost
    return costFunction.mean();
}
#endif /* SIMULATETREECUT_H */
