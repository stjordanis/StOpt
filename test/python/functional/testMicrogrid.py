
# Copyright (C) 2018 The Regents of the University of California, Michael Ludkovski and Aditya Maheshwari
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)


from __future__ import division
import numpy as np
import math
import matplotlib.pyplot as plt
import time
from datetime import datetime
import microGrid.parameters as bp
import microGrid.calEngine as ce
import microGrid.forwardSimulations as fs


def runSimulation():


	param = bp.basicVariables()

	ce.storageCalculation(param)


if __name__ == "__main__":

	runSimulation()	
	fs.forwardSimulations()
