// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  CONTINUATIONVALUEGENERS_H
#define  CONTINUATIONVALUEGENERS_H
#include <Eigen/Dense>
#include "geners/GenericIO.hh"
#include "StOpt/regression/ContinuationValue.h"
#include "StOpt/regression/BaseRegressionGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "StOpt/regression/SparseRegressionGeners.h"
#include "StOpt/core/grids/SpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/grids/SparseSpaceGridNoBoundGeners.h"
#include "StOpt/core/grids/SparseSpaceGridBoundGeners.h"

/** \file ContinuationValueGeners.h
 * \brief Define non intrusive serialization with random access
*  \author Xavier Warin
 */

/// specialize the ClassIdSpecialization template
/// so that a ClassId object can be associated with the class we want to
/// serialize.  The second argument is the version number.
///@{
gs_specialize_class_id(StOpt::ContinuationValue, 1)
/// an external class
gs_declare_type_external(StOpt::ContinuationValue)
///@}

namespace gs
{
//
/// \brief  This is how the specialization of GenericWriter should look like
//
template <class Stream, class State >
struct GenericWriter < Stream, State, StOpt::ContinuationValue,
           Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool process(const StOpt::ContinuationValue  &p_regression, Stream &p_os,
                               State *, const bool p_processClassId)
    {
        // If necessary, serialize the class id
        static const ClassId current(ClassId::makeId<StOpt::ContinuationValue >());
        const bool status = p_processClassId ? ClassId::makeId<StOpt::ContinuationValue >().write(p_os) : true;
        // Serialize object data if the class id was successfully
        // written out
        if (status)
        {
            int isizeRows = p_regression.getValues().rows();
            int isizeCols = p_regression.getValues().cols();
            write_pod(p_os, isizeRows);
            write_pod(p_os, isizeCols);
            write_pod_array(p_os, p_regression.getValues().data(), isizeRows * isizeCols);
            std::shared_ptr< StOpt::SpaceGrid > ptrGrid = p_regression.getGrid();
            bool bSharedPtr = (ptrGrid ? true : false);
            write_pod(p_os, bSharedPtr);
            if (bSharedPtr)
                write_item(p_os, *p_regression.getGrid());
            write_item(p_os, *p_regression.getCondExp());
        }
        // Return "true" on success, "false" on failure
        return status && !p_os.fail();
    }
};

/// \brief  And this is the specialization of GenericReader
//
template <class Stream, class State  >
struct GenericReader < Stream, State, StOpt::ContinuationValue, Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool readIntoPtr(StOpt::ContinuationValue  *&ptr, Stream &p_is,
                                   State *p_st, const bool p_processClassId)
    {

        if (p_processClassId)
        {
            static const ClassId current(ClassId::makeId<StOpt::ContinuationValue>());
            ClassId id(p_is, 1);
            current.ensureSameName(id);
        }

        /* // Deserialize object data. */
        int isizeRows = 0;
        read_pod(p_is, &isizeRows);
        int isizeCols = 0;
        read_pod(p_is, &isizeCols);
        int isizeLoc = isizeRows * isizeCols;
        Eigen::ArrayXXd values(isizeRows, isizeCols);
        read_pod_array(p_is, values.data(), isizeLoc);
        bool bSharedPtr ;
        read_pod(p_is, &bSharedPtr);
        CPP11_auto_ptr<StOpt::SpaceGrid> pgrid ;
        if (bSharedPtr)
            pgrid  = read_item<StOpt::SpaceGrid>(p_is);
        std::shared_ptr<StOpt::SpaceGrid > pgridShared(std::move(pgrid));
        CPP11_auto_ptr<StOpt::BaseRegression> pcond  = read_item<StOpt::BaseRegression>(p_is);
        std::shared_ptr<StOpt::BaseRegression> pcondShared(std::move(pcond));
        if (p_is.fail())
            // Return "false" on failure
            return false;
        //Build the object from the stored data
        if (ptr)
        {
            *ptr = StOpt::ContinuationValue();
            ptr->loadForSimulation(pgridShared, pcondShared, values);
        }
        else
            ptr = new  StOpt::ContinuationValue(pgridShared, pcondShared, values);
        return true;
    }

    inline static bool process(StOpt::ContinuationValue &s, Stream &is,
                               State *st, const bool p_processClassId)
    {
        // Simply convert reading by reference into reading by pointer
        StOpt::ContinuationValue *ps = &s;
        return readIntoPtr(ps, is, st, p_processClassId);
    }
};
}



#endif/*  CONTINUATIONVALUEGENERS_H */
