// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALLINEARREGRESSIONGENERS_H
#define LOCALLINEARREGRESSIONGENERS_H
#include <geners/AbsReaderWriter.hh>
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/regression/BaseRegressionGeners.h"

/** \file LocalLinearRegressionGeners.h
 * \brief Define non intrusive  serialization with random access
*  \author Xavier Warin
 */

// Concrete reader/writer for class LocalLinearRegression
// Note publication of LocalLinearRegression as "wrapped_type".
struct LocalLinearRegressionGeners: public gs::AbsReaderWriter<StOpt::BaseRegression>
{
    typedef StOpt::BaseRegression wrapped_base;
    typedef StOpt::LocalLinearRegression wrapped_type;

    // Methods that have to be overridden from the base
    bool write(std::ostream &, const wrapped_base &, bool p_dumpId) const override;
    wrapped_type *read(const gs::ClassId &p_id, std::istream &p_in) const override;

    // The class id forLocalLinearRegression  will be needed both in the "read" and "write"
    // methods. Because of this, we will just return it from one static
    // function.
    static const gs::ClassId &wrappedClassId();
};

gs_specialize_class_id(StOpt::LocalLinearRegression, 1)
gs_declare_type_external(StOpt::LocalLinearRegression)
gs_associate_serialization_factory(StOpt::LocalLinearRegression, StaticSerializationFactoryForBaseRegression)

#endif  /* LOCALLINEARREGRESSIONGENERS_H */
