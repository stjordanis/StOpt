// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALCONSTREGRESSIONGENERS_H
#define LOCALCONSTREGRESSIONGENERS_H
#include <geners/AbsReaderWriter.hh>
#include "StOpt/regression/LocalConstRegression.h"
#include "StOpt/regression/BaseRegressionGeners.h"

/** \file LocalConstRegressionGeners.h
 * \brief Define non intrusive  serialization with random access
*  \author Xavier Warin
 */

// Concrete reader/writer for class LocalConstRegression
// Note publication of LocalConstRegression as "wrapped_type".
struct LocalConstRegressionGeners: public gs::AbsReaderWriter<StOpt::BaseRegression>
{
    typedef StOpt::BaseRegression wrapped_base;
    typedef StOpt::LocalConstRegression wrapped_type;

    // Methods that have to be overridden from the base
    bool write(std::ostream &, const wrapped_base &, bool p_dumpId) const override;
    wrapped_type *read(const gs::ClassId &p_id, std::istream &p_in) const override;

    // The class id forLocalConstRegression  will be needed both in the "read" and "write"
    // methods. Because of this, we will just return it from one static
    // function.
    static const gs::ClassId &wrappedClassId();
};

gs_specialize_class_id(StOpt::LocalConstRegression, 1)
gs_declare_type_external(StOpt::LocalConstRegression)
gs_associate_serialization_factory(StOpt::LocalConstRegression, StaticSerializationFactoryForBaseRegression)

#endif  /* LOCALCONSTREGRESSIONGENERS_H */
