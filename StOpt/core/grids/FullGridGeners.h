// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef FULLGRIDGENERS_H
#define FULLGRIDGENERS_H
#include "StOpt/core/grids/FullGrid.h"
#include <geners/AbsReaderWriter.hh>
#include <geners/associate_serialization_factory.hh>

/** \file FullGridGeners.h
 * \brief Define non intrusive  serialization with random acces
*  \author Xavier Warin
 */

/// \¢lass
///  I/O factory for classes derived from .
// Note publication of the base class and absence of public constructors.
class SerializationFactoryForFullGrid : public gs::DefaultReaderWriter<StOpt::FullGrid>
{
    typedef DefaultReaderWriter<StOpt::FullGrid> Base;
    friend class gs::StaticReaderWriter<SerializationFactoryForFullGrid>;
    SerializationFactoryForFullGrid();
};

// SerializationFactoryForFullGrid wrapped into a singleton
typedef gs::StaticReaderWriter<SerializationFactoryForFullGrid> StaticSerializationFactoryForFullGrid;

gs_specialize_class_id(StOpt::FullGrid, 1)
gs_declare_type_external(StOpt::FullGrid)
gs_associate_serialization_factory(StOpt::FullGrid, StaticSerializationFactoryForFullGrid)

#endif  /* FULLGRIDGENERS_H */
