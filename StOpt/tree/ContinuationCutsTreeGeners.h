// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  CONTINUATIONCUTSTREEGENERS_H
#define  CONTINUATIONCUTSTREEGENERS_H
#include <Eigen/Dense>
#include "geners/GenericIO.hh"
#include "geners/vectorIO.hh"
#include "StOpt/tree/ContinuationCutsTree.h"
#include "StOpt/tree/TreeGeners.h"
#include "StOpt/core/grids/SpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/utils/eigenGeners.h"

/** \file ContinuationCutsTreeGeners.h
 * \brief Define non intrusive serialization with random access
*  \author Xavier Warin
 */

/// specialize the ClassIdSpecialization template
/// so that a ClassId object can be associated with the class we want to
/// serialize.  The second argument is the version number.
///@{
gs_specialize_class_id(StOpt::ContinuationCutsTree, 1)
/// an external class
gs_declare_type_external(StOpt::ContinuationCutsTree)
///@}

namespace gs
{
//
/// \brief  This is how the specialization of GenericWriter should look like
//
template <class Stream, class State >
struct GenericWriter < Stream, State, StOpt::ContinuationCutsTree,
           Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool process(const StOpt::ContinuationCutsTree  &p_cutTree, Stream &p_os,
                               State *, const bool p_processClassId)
    {
        // If necessary, serialize the class id
        static const ClassId current(ClassId::makeId<StOpt::ContinuationCutsTree >());
        const bool status = p_processClassId ? ClassId::makeId<StOpt::ContinuationCutsTree >().write(p_os) : true;
        // Serialize object data if the class id was successfully
        // written out
        if (status)
        {
            std::shared_ptr< StOpt::SpaceGrid > ptrGrid = p_cutTree.getGrid();
            bool bSharedPtr = (ptrGrid ? true : false);
            write_pod(p_os, bSharedPtr);
            if (bSharedPtr)
                write_item(p_os, *p_cutTree.getGrid());
            write_item(p_os, p_cutTree.getValues());
        }
        // Return "true" on success, "false" on failure
        return status && !p_os.fail();
    }
};

/// \brief  And this is the specialization of GenericReader
//
template <class Stream, class State  >
struct GenericReader < Stream, State, StOpt::ContinuationCutsTree, Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool readIntoPtr(StOpt::ContinuationCutsTree  *&ptr, Stream &p_is,
                                   State *p_st, const bool p_processClassId)
    {

        if (p_processClassId)
        {
            static const ClassId current(ClassId::makeId<StOpt::ContinuationCutsTree>());
            ClassId id(p_is, 1);
            current.ensureSameName(id);
        }

        // Deserialize object data.
        bool bSharedPtr ;
        read_pod(p_is, &bSharedPtr);
        std::unique_ptr<StOpt::SpaceGrid> pgrid ;
        if (bSharedPtr)
            pgrid  = read_item<StOpt::SpaceGrid>(p_is);
        std::shared_ptr<StOpt::SpaceGrid > pgridShared(std::move(pgrid));
        std::unique_ptr< std::vector< Eigen::ArrayXXd > > cutCoeff = read_item<  std::vector< Eigen::ArrayXXd > >(p_is);

        if (p_is.fail())
            // Return "false" on failure
            return false;
        //Build the object from the stored data
        if (ptr)
        {
            *ptr = StOpt::ContinuationCutsTree();
            ptr->loadForSimulation(pgridShared, *cutCoeff) ; // values);
            return true;
        }
        return false;
    }

    inline static bool process(StOpt::ContinuationCutsTree &s, Stream &is,
                               State *st, const bool p_processClassId)
    {
        // Simply convert reading by reference into reading by pointer
        StOpt::ContinuationCutsTree *ps = &s;
        return readIntoPtr(ps, is, st, p_processClassId);
    }
};
}

#endif/*  CONTINUATIONCUTSTREEGENERS_H */
