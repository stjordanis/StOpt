// Copyright (C) 2017 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <memory>
#include <Rcpp.h>
#include <RcppEigen.h>
#include <Eigen/Dense>
#include "StOpt/core/utils/Polynomials1D.h"
#include "StOpt/regression/gridKernelRegression.h"
#include "StOpt/regression/LocalGridKernelRegression.h"
#include "StOpt/regression/LocalConstRegression.h"
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/regression/LocalSameSizeConstRegression.h"
#include "StOpt/regression/LocalSameSizeLinearRegression.h"
#include "StOpt/regression/SparseRegression.h"
#include "StOpt/regression/GlobalRegression.h"
#include "StOpt/regression/LaplacianConstKernelRegression.h"
#include "StOpt/regression/LaplacianLinearKernelRegression.h"


using namespace Rcpp ;
using namespace Eigen ;
using namespace std ;




RCPP_MODULE(StOptRegression)
{

    class_< StOpt::LocalLinearRegression>("LocalLinearRegression")
    .constructor< bool, ArrayXXd, ArrayXi, bool >("Default constructor : true if only average, particles matrix, number of meshes in each direction")

    .method("getAllSimulations", & StOpt::LocalLinearRegression::getAllSimulations, "Regress the given vector")
    ;
    class_<StOpt::LocalConstRegression>("LocalConstRegression")
    .constructor< bool, ArrayXXd, ArrayXi, bool >("Default constructor : true if only average, particles matrix, number of meshes in each direction")

    .method("getAllSimulations", &StOpt::LocalConstRegression::getAllSimulations, "Regress the given vector")
    ;

    class_<StOpt::LocalSameSizeLinearRegression>("LocalSameSizeLinearRegression")
    .constructor< bool, ArrayXXd, ArrayXd, ArrayXd, ArrayXi>("Default constructor : true if only average, particles matrix, low values, steps and number of meshes in each direction")

    .method("getAllSimulations", &StOpt::LocalSameSizeLinearRegression::getAllSimulations, "Regress the given vector")
    ;

    class_<StOpt::LocalSameSizeConstRegression>("LocalSameSizeConstRegression")
    .constructor< bool, ArrayXXd, ArrayXd, ArrayXd, ArrayXi >("Default constructor : true if only average, particles matrix, low values, steps and  number of meshes in each direction")

    .method("getAllSimulations", &StOpt::LocalSameSizeConstRegression::getAllSimulations, "Regress the given vector")
    ;


    class_<StOpt::GlobalRegression<StOpt::Hermite>>("GlobalHermiteRegression")
            .constructor<  bool, ArrayXXd, int, bool>("Default constructor : true if only average, particles matrix, global polynmial degree")

            .method("getAllSimulations", &StOpt::GlobalRegression<StOpt::Hermite>::getAllSimulations, "Regress the given vector")
            ;

    class_<StOpt::GlobalRegression<StOpt::Canonical> >("GlobalCanonicalRegression")
    .constructor<  bool, ArrayXXd, int, bool>("Default constructor : true if only average, particles matrix, global polynmial degree")

    .method("getAllSimulations", &StOpt::GlobalRegression<StOpt::Canonical>::getAllSimulations, "Regress the given vector")
    ;

    class_<StOpt::GlobalRegression<StOpt::Tchebychev>>("GlobalTchebychevRegression")
            .constructor<  bool, ArrayXXd, int, bool>("Default constructor : true if only average, particles matrix, global polynmial degree")

            .method("getAllSimulations", &StOpt::GlobalRegression<StOpt::Tchebychev>::getAllSimulations, "Regress the given vector")
            ;


    class_<StOpt::SparseRegression>("SparseRegression")
    .constructor< bool, ArrayXXd,  int, ArrayXd,  int >("Default constructor :  true if only average, particles matrix, sparse level ,weights, degree of the interpolator")

    .method("getAllSimulations", &StOpt::SparseRegression::getAllSimulations, "Regress the given vector")
    ;


    class_<StOpt::LocalGridKernelRegression>("LocalGridKernelRegression")
    .constructor< bool, ArrayXXd,  double, double, bool >("Default constructor :  true if only average, particles matrix, bandwidth coefficient, grid points coefficient")
    .method("getAllSimulations", &StOpt::LocalGridKernelRegression::getAllSimulations, "Regress the given vector")
    ;


    class_<StOpt::LaplacianConstKernelRegression>("LaplacianConstKernelRegression")
    .constructor< bool, ArrayXXd,  ArrayXd >("Default constructor :  true if only average, particles matrix, bandwidth coefficient, grid points coefficient")
    .method("getAllSimulations", &StOpt::LaplacianConstKernelRegression::getAllSimulations, "Regress the given vector")
    ;

    class_<StOpt::LaplacianLinearKernelRegression>("LaplacianLinearKernelRegression")
    .constructor< bool, ArrayXXd,  ArrayXd >("Default constructor :  true if only average, particles matrix, bandwidth coefficient, grid points coefficient")
    .method("getAllSimulations", &StOpt::LaplacianLinearKernelRegression::getAllSimulations, "Regress the given vector")
    ;

}


